package be.vdab.jodemuynck;

import java.util.Observable;
import java.util.Observer;

public class Inwoner implements Observer {

    private String naam;
    VluchtReactie vluchten;

    Inwoner(String naam){
        this.naam = naam;
    }

    @Override
    public void update(Observable o, Object arg) {
        vluchten.vluchtreactie();
    }
}
