package be.vdab.jodemuynck;

public enum InwonerFactory {
    
    INSTANCE;

    Inwoner Create(String tekstregel){
        
        Inwoner inwoner = null;
        char soort = tekstregel.charAt(0);
        String naam = tekstregel.substring(1);
        
        switch (soort){
            case 'V':
                inwoner = new Vogel(naam);
                break;
            case 'Z':
                inwoner = new Zoogdier(naam);
                break;
            case 'R':
                inwoner = new Reptiel(naam);
                break;
        }
        return inwoner;
    }
    
}
