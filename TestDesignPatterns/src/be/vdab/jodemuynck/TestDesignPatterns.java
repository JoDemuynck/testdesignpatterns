package be.vdab.jodemuynck;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestDesignPatterns {

    public static void main(String[] args) {

        //Design pattern: inwoners Observen vulkaan.
        Vulkaan vulkaan = new Vulkaan();
        List<Inwoner> inwoners = new ArrayList<>();
        Inwoner inwoner;

        try {
            List<String> regels = Files.readAllLines(Paths.get("/data/inwoners.txt"));
            for (String regel : regels) {
                //Design pattern: inwoners zijn verschillende objecten gemaakt in Factory.
                //Design pattern: Factory is een Singelton.
                //Design pattern: inwoners hebben eigen vlucht Strategy om op vulkaan te reageren.
                inwoner = InwonerFactory.INSTANCE.Create(regel);
                inwoners.add(inwoner);
                vulkaan.addObserver(inwoner);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }

        vulkaan.uitbarsten();
    }

}
