package be.vdab.jodemuynck;

public class Reptiel extends Inwoner {
    
    Reptiel(String naam){
        super(naam);
        vluchten = ()->System.out.println(String.format("%s kruipt onder een steen.", naam));
    }
    
}
