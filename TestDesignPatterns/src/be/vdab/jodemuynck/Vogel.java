package be.vdab.jodemuynck;

public class Vogel extends Inwoner {
    
    Vogel(String naam){
        super(naam);
        vluchten = ()->System.out.println(String.format("%s gaat hoger vliegen.", naam));
    }
    
}
