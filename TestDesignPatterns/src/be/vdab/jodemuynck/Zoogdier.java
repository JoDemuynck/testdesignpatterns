package be.vdab.jodemuynck;

public class Zoogdier extends Inwoner {
    
    Zoogdier(String naam){
        super(naam);
        vluchten = ()->System.out.println(String.format("%s kruipt in zijn hol.", naam));
    }
    
}
