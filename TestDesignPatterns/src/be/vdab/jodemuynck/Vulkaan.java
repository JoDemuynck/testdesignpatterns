package be.vdab.jodemuynck;

import java.util.Observable;

public class Vulkaan extends Observable {
    
    Vulkaan(){
        System.out.println("Alles is rustig...");
    }
    void uitbarsten(){
        System.err.println("Vulkaanuitbarsting!");
        setChanged();
        notifyObservers();
    }
    
}
